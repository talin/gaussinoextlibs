set(HEPMC3_GIT_SOURCE "https://gitlab.cern.ch/Gaussino/HepMC3.git" CACHE STRING "URL of the HepMC3 git repository")
set(HEPMC3_GIT_TAG "official_patched" CACHE STRING "Tag of HepMC3 to be used")
set(DD4hep_GIT_SOURCE "https://gitlab.cern.ch/Gaussino/DD4hep.git" CACHE STRING "URL of the DD4hep git repository")
set(DD4hep_GIT_TAG "v01-25-01-patches" CACHE STRING "Tag of DD4hep to be used")
